#Ejercicio 2
#Antonella Marabotto y Melina Vladisauskas

import sys
import random
import time
archivo = sys.argv[1]

def listaDePuntos(archivo): #esta funcion toma una lista de puntos y devuelve 
#una lista de tuplas de esos puntos
    a = []
    with open(archivo,'r') as inf: #abro y leo el archivo con los puntos
        for line in inf:
            m = line.split(' ') #separo en cada linea el valor de x del de y
            for i in range (0,len(m)): 
                m[i] = float(m[i]) #transformo en flotantes a todos los 
#elementos de cada fila
            a.append((m[0],m[1])) #meto los valores de x e y en tuplas
    return a
listaDePuntos(archivo)

def distanciaMinima(a): #compara punto a punto la distancia minima y devuelve 
#la tupla cuyos puntos sean mas cercanos
    distancia_minima = ((a[1][0] - a[0][0])**2 + (a[1][1] - a[0][1])**2)**(0.5)
#calculo la distancia entre la primera tupla
    tupla_minima = [a[0],a[1]] #la defino como minima   
    for i in range (0,len(a)-1): #recorro todos los pares de tuplas en sus 
#elementos x,y, y hago el calculo de distancia para cada punto
        for j in range (i+1,len(a)):
            distancia = ((a[j][0] - a[i][0])**2 + (a[j][1] - a[i][1])**2)**(0.5)
            if distancia_minima > distancia: #comparo la nueva distancia con 
#la anterior, y si era menor almaceno la nueva tupla como la minima
                distancia_minima=distancia                
                tupla_minima = [a[i],a[j]]
    return tupla_minima

#tupla1 = distanciaMinima(listaDePuntos(archivo))
#t1=time.time()
#print(distanciaMinima(listaDePuntos(archivo)))
#t2=time.time()
#print(t2-t1)
   
def upsortAux(a,x,y): #devuelve la posición del valor de x mayor entre dos tuplas
    l = x
    for i in range(x,y+1): #comparo los valores de x de dos tuplas
        if a[l][0] < a[i][0]: #si el valor en la posición i es mayor, la 
#funcion devuelve esa posición
            l = i
    return l
def upsort(a): #funcion que aplica la funcion upsort al parametro a, la lista 
#de puntos
    total = len(a)-1
    n = 0
    while total > 0:
        n = upsortAux(a, 0, total)
        a[n], a[total] = a[total], a[n]
        total = total-1
    return a

def mergeSort(a,x,y): #a es la lista (o fragmento de lista), x e y son indices 
#del primer y ultimo elemento respectivamente
    if y-x==0: #si la lista es de un numero, devuelve ese numero
        return a
    if y-x==1: #si la lista tiene dos elementos, los reordeno según su valor
        if a[x]>a[y]:  
            a[x],a[y]=a[y],a[x]
        return a
    z=((x+y)//2)  #si la lista tiene mas de dos elementos, la parto de nuevo 
#por el elemento del indice del medio entre x e y
    mergeSort(a,x,z) #aplico recursion hasta que y-x = 0
    mergeSort(a,z+1,y) #aplico recursion en la nueva lista generada
    merge(a,x,z,y) 
    return a
    
def merge(a,x,z,y): #funcion que dada una lista de pares de puntos, y 3 indices
#de listas (explicados arriba), une los fragmentos de listas resultantes de la 
#funcion mergesort() de menor a mayor
    p=x #indice del primer elemento
    q=z+1 #indice del elemento del final de una lista
    a_final=[]
    while p<z+1 and q<=y: # repito el proceso recorriendo ambas listas, 
#comparando el primer elemento de ambas y apendando al menor
        if a[p][0] < a[q][0]:
            a_final.append(a[p])
            p+=1
        else:
            a_final.append(a[q])
            q+=1
    if p==z+1: #si llegue al final de alguna de las dos listas, le sumo el 
#resto a a_final
        a_final=a_final+a[q:(y+1)]
    else:
        a_final=a_final+a[p:(z+1)]
    a[x:(y+1)]=a_final
    return a

def Distancia_minima(t1, t2): #funcion que calcula la distancia minima entre 
#dos puntos
    return ((t1[0] - t2[0])**2 + (t1[1] - t2[1])**2)**(0.5)

def DyCAuxiliar(a, x, y):
    cota=0
    if y-x==2:
        t=distanciaMinima(a[x:(y+1)])
        return t
    elif y-x==1:
        return a[x:(y+1)]
    z=(x+y+1)//2 #hasta aca es practicamente igual a Mergesort
    izquierda = DyCAuxiliar(a, x, z-1) #aplico recursivamente la funcion a 
#ambas porciones nuevas
    derecha = DyCAuxiliar(a, z, y)
    eq_Cota=None
    mitad=(a[z][0]+a[z-1][0])/2 #recta que corta ambas mitades
    if Distancia_minima(izquierda[0], izquierda[1]) < Distancia_minima(derecha[0], derecha[1]):
#si la minima esta en la porcion izquierda, se la asigna a la cota, de lo contrario se le asigna la derecha
        cota=Distancia_minima(izquierda[0], izquierda[1])
        eq_Cota=izquierda
    else:
        cota=Distancia_minima(derecha[0], derecha[1])
        eq_Cota=derecha
    if cota==0: #si las distancias minimas son iguales en ambas porciones
        return eq_Cota
    p=x
    q=y
    while a[p][0]<(mitad-cota): #uso la cota para determinar cuanto me tengo 
#que alejar hacia los costados 
        p = p + 1
    while a[q][0]>(mitad+cota):
        q = q - 1
    if p<q:
        par_medio=distanciaMinima(a[p:(q+1)])
        if Distancia_minima(par_medio[0], par_medio[1])<Distancia_minima(eq_Cota[0], eq_Cota[1]):
            return par_medio #si la distancia del par central es menor que la 
#de la cota, el resultado es ese par, de lo contrario es el de la cota
    return eq_Cota

def distanciaMinimaDyC(a,algoritmo): #funcion final en la que se decide el 
#algoritmo a utilizar
    x=0
    y=len(a)-1
    if algoritmo == 'merge':
        mergeSort(a, 0, y)
    elif algoritmo=='up':
        upsort(a)
    else:
        raise NameError
    m=time.time()

    res=DyCAuxiliar(a, x, y) #llamamos a la funcion auxiliar de DyC
    n=time.time()
    return res
    
#tupla2= distanciaMinimaDyC(listaDePuntos(archivo),'merge')
#ti = time.time()
#print(distanciaMinimaDyC(listaDePuntos(archivo),'merge'))
#tf = time.time()
#print (tf-ti)

#print(Distancia_minima((tupla2[0]),tupla2[1])) #Esto es para evaluar si la distancia entre las dos tuplas de resultado es la misma
#print(Distancia_minima((tupla1[0]),tupla1[1]))

####### COMANDOS DE EJECUCION
archivo = sys.argv[1]
listaDePuntos(archivo)

t1=time.time()
print(distanciaMinima(listaDePuntos(archivo)))
t2=time.time()
print(t2-t1)

ti = time.time()
print(distanciaMinimaDyC(listaDePuntos(archivo),'merge')) #o "merge"
tf = time.time()
print (tf-ti)


