#include "filters.h"
#include <stdio.h>
//im res Imagen resultante (escala de grises).
//im Imagen origen (escala de grises).
//ii Cantidad de filas de im y de im res.
//jj Cantidad de columnas de im y de im res.
void blur_filter(float * im_res, float * im, int ii, int jj){
    for (int z=1; z<ii-1; z=z+1){
        for (int y=1;y<jj-1; y=y+1){
            im_res[z*jj+y]=(im[((z-1)*jj)+y]+im[((z+1)*jj)+y]+im[z*jj+(y-1)]+im[z*jj+(y+1)])/4;
        }
    }
}
