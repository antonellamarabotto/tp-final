# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 10:52:21 2017

@author: Antonella
"""

###Trabajo Practico Final: Atonella Marabotto y Melina Vladisauskas

##Ejercicio 1: Arboles binarios

import unittest ##Vamos a testear usando unittest
        
class ArbolBinario:#Creo una clase de objeto nueva. Contiene todas las funciones a continuacion.
    def __init__(self):#Seteo las parametros iniciales, el arbol es vacio.
        self.der=None
        self.izq=None
        self.root=None
    def vacio(self):#Devuelvo True si el arbol es vacio
        if self.root==None:
            return True
        else:
            return False
        
    def raiz(self):#Devuelvo la raiz o falso si no tiene
        return self.root
    
    def bin(self,raiz, izq, der):#Defino la estructura del arbol
        self.root=raiz#Defino la raiz del arbol
        self.der=der#Defino la rama derecha
        self.izq=izq#Defino la rama izquierda
          
    def izquierda(self):#Devuelvo la rama izquierda 
        return self.izq
    
    def derecha(self):#Devuelvo la rama derecha
        return self.der
    
    def find(self,a):#Busca si hay un elemento "a" en el arbol
        if self.vacio():
            return False
        if self.root==a:
            return True
        return self.izq.find(a) or self.der.find(a)#Busco en todos los elementos de las ramas derecha e izquierda hasta que aparece "a"
    
    def espejo(self):
        c=ArbolBinario()#Creo un arbol vacio para despues agregarle las ramaz izq y der del arbol original de forma invertida
        if self.vacio():
            return c#Devuelvo un arbol vacio.
        else:
            c.bin(self.root,self.derecha().espejo(),self.izquierda().espejo())#Pongo la rama derecha en donde va la izquierda, y la izquierda donde va la derecha.
            return c#Devuelvo el nuevo arbol

    def preorder(self):#Devuelvo el arbol como una lista poniendo primero la raiz,a despues la rama izq y al final la derecha
        listapre=[]
        if self.root!=None:
            return [self.root]+self.izq.preorder()+self.der.preorder()
        else:
            return listapre
    
    
    def inorder(self):#devuelvo el arbol como una lista poniendo primero rama izq, despues la raiz y al final la rama der
        listain=[]
        if self.root!=None:
            return self.izq.inorder()+[self.root]+self.der.inorder()
        else:
            return listain
        
    
    def postorder(self):#devuelvo el arbol como una lista poniendo primero rama izq, dps rama der y al final la raiz
        listapost=[]
        if self.root!=None:
            return self.izq.postorder()+self.der.postorder()+[self.root]#Raiz no es una lista, mientras que las ramas izquierda y derecha si,por eso agrego corchetes a raiz.               
        else:        
            return listapost


###################Testing usando unitest
class testeoArbolBinario(unittest.TestCase):
    
    def __init__(self, *args, **kwargs):
        ##Creo un arbol binario para el testeo
        a=ArbolBinario()#a es una instancia de la clase arbol binario que es vacio por como esta definido en init
        a.bin(1,ArbolBinario(),ArbolBinario())
        b=ArbolBinario()
        b.bin(4,ArbolBinario(),ArbolBinario())
        c=ArbolBinario()
        c.bin(2,a,b)#Completo el arbol c con el arbol a en la rama izquierda y el arbol b en la rama derecha.
        d=ArbolBinario()
        d.bin(3,c,ArbolBinario())
    
        super(testeoArbolBinario, self).__init__(*args, **kwargs)
        self.arbol = c
        
    def test_raiz(self):#Falla si la raiz del arbol no es 2
        self.assertEqual(self.arbol.raiz(),2)
    def test_find(self):
        self.assertTrue(self.arbol.find(1))#Falla si el 1 no esta en el arbol.
        self.assertFalse(self.arbol.find(12312))#Falla si el numero esta en el arbol
        self.assertTrue(self.arbol.find(4))# Falla si el 4 no esta en el arbol
    def test_espejo(self):  
        a=ArbolBinario()#a es una instancia de la clase arbol binario que es vacio por como esta definido en init
        a.bin(1,ArbolBinario(),ArbolBinario())
        b=ArbolBinario()
        b.bin(4,ArbolBinario(),ArbolBinario())
        c=ArbolBinario()
        c.bin(2,a,b)#Completo el arbol c con el arbol a en la rama izquierda y el arbol b en la rama derecha.
        d=ArbolBinario()
        d.bin(3,c,ArbolBinario())
        arbol=d
        ramaderechaespejo=print(arbol.espejo().derecha())
        ramaizquierda=print(arbol.espejo().izquierda())
        self.assertEqual(ramaderechaespejo,ramaizquierda)#La rama izquierda de "arbol" es igual a la rama derecha del arbol espejado.
        
###############Inorder, preorder y postorder
    def test_preorder(self):#Creo un arbol y verifico que cuando lo imprimo el primer elemento es la rama izquierda, el segundo la rama derecha, y el tercero la raiz
        a=ArbolBinario()
        a.bin(1,ArbolBinario(),ArbolBinario())
        b=ArbolBinario()
        b.bin(2,ArbolBinario(),ArbolBinario())
        c=ArbolBinario()
        c.bin(3,a,b)#Completo el arbol"c" con el arbol "a" en la rama izquierda y el arbol "b" en la rama derecha.
        Arbol=c.preorder()
        d=Arbol[0]
        e=Arbol[1]
        f=Arbol[2]
        self.assertEqual(d,3)#Falla si el elemento no es la Raiz
        self.assertEqual(e,1)#Falla si el elemento no es de laRama izquierda
        self.assertEqual(f,2)#Falla si el elemento no es de la Rama derecha
    
    def test_inorder(self):#Mismo testeo que preorder, pero va primero la rama izquierda, despues la raiz y al final la rama derecha.
        a=ArbolBinario()
        a.bin(1,ArbolBinario(),ArbolBinario())
        b=ArbolBinario()
        b.bin(2,ArbolBinario(),ArbolBinario())
        c=ArbolBinario()
        c.bin(3,a,b)#Completo el arbol"c" con el arbol "a" en la rama izquierda y el arbol "b" en la rama derecha.
        Arbol=c.inorder()
        d=Arbol[0]
        e=Arbol[1]
        f=Arbol[2]
        self.assertEqual(d,1)#Falla si el elemento no es el de la Rama izquierda
        self.assertEqual(e,3)#falla si el elemento no es la raiz
        self.assertEqual(f,2)#Falla si el elemento no es de la rama derecha.
        
        
    def test_postorder(self):#Mismo testeo que preorder, pero va primero la rama izquierda, despues la derecha y al final la raiz.
        a=ArbolBinario()
        a.bin(1,ArbolBinario(),ArbolBinario())
        b=ArbolBinario()
        b.bin(2,ArbolBinario(),ArbolBinario())
        c=ArbolBinario()
        c.bin(3,a,b)#Completo el arbol"c" con el arbol "a" en la rama izquierda y el arbol "b" en la rama derecha.
        Arbol=c.postorder()
        d=Arbol[0]
        e=Arbol[1]
        f=Arbol[2]
        self.assertEqual(d,1)#Falla si el elemento no es de la Rama izquierda
        self.assertEqual(e,2)#Falla si el elemento no es de la Rama derecha
        self.assertEqual(f,3)#Falla si el elemento no es la Raiz

        

if __name__ == "__main__":
    unittest.main(verbosity=2)