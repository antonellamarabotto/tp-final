﻿###Trabajo Practico Final: Atonella Marabotto y Melina Vladisauskas

##Ejercicio 3


#Librerias a utilizar
import imfilters
from scipy import misc
import numpy as np
import time

##Abro una imagen
ImA=misc.imread("fondo.jpg")
ImB=misc.imread("EscaladaenHielo.jpeg")
ImC=misc.imread("bicicleta.jpg")
#Guardo la imagen
#ImgA=misc.imsave('fondo.png', ImA)
#ImgB=misc.imsave('EscaladaenHielo.png', ImB)
#ImgC=misc.imsave('bicicleta.png', ImC)

          
##Definimos la funcion gray_filters que me devuelve un array que codifica una imagen en escala de grises     
def gray_filter(img):
    sizefila=len(img)#largo de las filas
    sizecol=len(img[0])#largo de las columnas
    resgray=np.empty([sizefila,sizecol])#Creo una matriz de numPy vacia con las mismas proporciones que img
    for i in range (0,sizefila):        
        for j in range (0,sizecol):
            resgray[i][j]=(0.3*img[i][j][0]+0.5*img[i][j][1]+0.11*img[i][j][2])
    misc.imsave("Imagen_Filtro_Gray.png", resgray)#Guardo la matriz generada con el filtro gray_filter
    return resgray

def blur_filter(resgray):#Filtro Blur: Borroso
    sizefilablur=len(resgray)
    sizecolblur=len(resgray[0])    
    resblur=np.zeros((sizefilablur,sizecolblur), dtype=object)#Creo una matriz vacia del mismo tamaño que la imagen.

    for i in range (1,sizefilablur-1):# Empiezo a recorrer desde la segunda fila y termino una fila antes por como esta definida la funcion.
        for j in range (1,sizecolblur-1):#Empiezo a recorrer desde la segunda columna y termino una columna antes por como esta definida la funcion.
            resblur[i][j]=(resgray[i-1][j]+resgray[i+1][j]+resgray[i][j-1]+resgray[i][j+1])/4
    misc.imsave("Imagen_Filtro_Blur.png", resblur)#Guardo la matriz generada con el filtro blur_filter

 
def ImageProcesing(img):#Creo una funcion que procese con los filtros creados las imagenes
    #Proceso la imagen con python
    t_inicial_Python=time.time()
    gray_filter(img)#Proceso con filtro gris
    blur_filter(gray_filter(img))#Proceso con filtro borroso
    t_final_Python=time.time()
    tiempoPython=t_final_Python-t_inicial_Python#Calculo el tiempo de procesamiento de la imagen en Python  
    #Proceso la imagen con C++
    t_inicial_C=time.time()
    gray_filterCpp=imfilters.gray_filter(img)#Proceso con filtro gris
    misc.imsave("grayfilterCpp"+img,gray_filterCpp)#Guardo la imagen
    blur_filterCpp=imfilters.blur_filter(gray_filterCpp)#Proceso con filtro borroso
    misc.imsave("blurfiltercpp"+img,blur_filterCpp)
    t_final_C=time.time()
    tiempoC=t_final_C-t_inicial_C#Calculo el tiempo de pocesamietno de C
#    Proceso la Imagen con C++ y OpenMP
    t_inicial_C=time.time()
    gray_filterOpenMPCpp=imfilters.gray_filterOpenMP(img)#Proceso con filtro gris
    misc.imsave("grayfilterCppOpenMP"+img,gray_filterOpenMPCpp)#Guardo la imagen
    blur_filterOpenMPCpp=imfilters.blur_filterOpenMP(gray_filterOpenMPCpp)#Proceso con filtro borroso
    misc.imsave("blurfiltercppOpenMP"+img,blur_filterOpenMPCpp)
    t_final_C=time.time()
    tiempoCMP=t_final_C-t_inicial_C#Calculo el tiempo de pocesamietno de C
    
    return (tiempoPython,tiempoC,tiempoCMP)



ImagenA=ImageProcesing(ImA)
ImagenB=ImageProcesing(ImB)
ImagenC=ImageProcesing(ImC)
#Imagen A
print('Tiempo de procesado de la imagen con python:',ImagenA[0])
print('Tiempo de procesado de la imagen con C++:',ImagenA[1])
print('Tiempo de procesado de la imagen con C++:',ImagenA[2])

#Imagen B
print('Tiempo de procesado de la imagen con python:',ImagenB[0])
print('Tiempo de procesado de la imagen con C++:',ImagenB[1])
print('Tiempo de procesado de la imagen con C++:',ImagenB[2])

#Imagen C

print('Tiempo de procesado de la imagen con python:',ImagenC[0])
print('Tiempo de procesado de la imagen con C++:',ImagenC[1])
print('Tiempo de procesado de la imagen con C++:',ImagenC[2])