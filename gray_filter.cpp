#include "filters.h"

void gray_filter(float * im_res, pixel_t * im, int ii, int jj){
     for (int k=0;k<ii;k=k+1){
        for (int y=0;y<jj;++y){
            im_res[k*jj+y]= im[k*jj+y].r*0.3 + im[k*jj+y].g*0.6 + im[k*jj+y].b*0.11;
           }
        }
}
