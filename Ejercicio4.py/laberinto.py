#import sys
#fn=sys.argv[1]

class Laberinto(object): #defino la clase mi_laberinto, en la que incluyo todas los metodos necesarios para lograr que el laberinto funcione
    def __init__(self, parent=None): #defino el metodo del estado inicial
        self.size = 0,0
        self.parent = parent
    def cargar(self, fn): #abre, lee, procesa el archivo de texto cuyo nombre se indica en la variable fn, en el formato requerido
        self.matrix=[]
        self.list=[]
        with open(fn, 'r') as inf: #abrimos el txt en modo lectura
            for line in inf:
                self.list.append(line) #agregamos las lineas del archivo a la lista
            for i in range(1, len(self.list)): #recorremos la lista y cambiamos el formato del contenido para que sea legible por el programa
                elemento = self.list[i].split('][')
                elemento[0] = elemento[0][1:]
                elemento[-1] = elemento[-1][:-2]
                self.matrix.append(elemento)
            for i in range(len(self.matrix)):
                for j in range(len(self.matrix[i])):
                    self.matrix[i][j] = self.matrix[i][j].split(',')
                    for k in range(len(self.matrix[i][j])):
                        self.matrix[i][j][k]=int(self.matrix[i][j][k]) #convierto en entero cada elemento de la matriz
        self.row=len(self.matrix)
        self.column=len(self.matrix[0])
        self.size=(self.row, self.column)
        self.resetear()

    def tamano(self): #devuelve el tamanio de la matriz, en filas y en columnas
        return self.size

    def resetear(self): #funcion que resetea el laberinto, ubica a la rata en la posicion 0,0 y al queso a la posicion (filas − 1, columnas − 1) (borde inferior derecho)
        self.__diccionarios__()
        self.pos_rata=(0, 0)
        self.pos_queso=(self.row-1, self.column-1)

    def getPosicionRata(self): #devuelve una tupla de dos elementos: la interseccion de la matriz en la que se encuentra el raton
        return self.pos_rata

    def getPosicionQueso(self): #devuelve una tupla de dos elementos: la interseccion de la matriz en la que se encuentra el queso
        return self.pos_queso

    def setPosicionRata(self, i, j): #setea la posicion de la rata a i,j. Si es posible devuelve True.
        self.pos_rata=(i, j)
        return self.pos_rata==(i, j)

    def setPosicionQueso(self, i, j): #setea la posicion del queso a i,j. Si es posible devuelve True.
        self.pos_queso=(i, j)
        return self.pos_queso==(i, j)

    def esPosicionRata(self, i, j): #devuelve True si el par ordenado de la posicion i,j de la rata es donde esta la rata
        return self.pos_rata==(i, j)

    def esPosicionQueso(self, i, j): #devuelve True si el par ordenado de la posicion i, j del queso es donde esta el queso
        return self.pos_queso==(i, j)    
                
    def get(self, i, j): #devuelve una lista de elementos booleanos que indican si hay paredes alrededor de la celda en la posicion i,j.
        matriz_booleana=[]
        for k in range(len(self.matrix[i][j])):
            matriz_booleana.append(self.matrix[i][j][k]==1)
        return matriz_booleana
    
    def getInfoCelda(self, i, j): #devuelve informacion sobre la celda en la posicion i,j para dos variables booleanas que nos hablan de si esa celda ya fue visitada, y si forma parte del camino actual del backtracking
        return self.diccionarios[i][j]

    def resuelto(self): #esta resuelto si la posición de la rata es igual a la posicion del queso
        return self.pos_rata==self.pos_queso
        
    def __diccionariosAuxiliar__(self):
        fila=[]
        for k in range(self.size[1]):
            fila.append({'visitada': False, 'caminoActual': False})
        return fila

    def __diccionarios__(self): #esta funcion y la anterior brindan la informacion sobre la celda en la que esta el raton, modificando los valores de las variables booleanas "visitada" y "camino actual"
        self.diccionarios=[]
        for k in range(self.size[0]):
            self.diccionarios.append(self.__diccionariosAuxiliar__())
        self.diccionarios[0][0]['caminoActual']=True
        return self.diccionarios

    def ultimaPos(self, m, n, tupla):
        x=self.get(tupla[0], tupla[1])
        if not (m==1 and (tupla[0]==self.row-1 or x[3]==True)):
            if not (m==-1 and (tupla[0]==0 or x[1]==True)):
                if not (n==1 and (tupla[1]==self.column-1 or x[2]==True)):
                    if not (n==-1 and (tupla[1]==0 or x[0]==True)):
                        h=self.getInfoCelda(tupla[0]+m, tupla[1]+n)
                        if h['caminoActual']==True and (tupla[0]+m, tupla[1]+n)!=self.getPosicionRata():
                            return (tupla[0]+m, tupla[1]+n)
        else:
            return False

    def retroceder(self, lastPos): #permite que el raton retroceda de posicion si se cumple que ya visito una celda y que esa celda forma parte del camino actual
        self.diccionarios[self.getPosicionRata()[0]][self.getPosicionRata()[1]]['visitada']=True
        self.diccionarios[self.getPosicionRata()[0]][self.getPosicionRata()[1]]['caminoActual']=False
        self._redibujar()
        self.setPosicionRata(lastPos[0], lastPos[1])
        self._redibujar()

    def resolverAux(self, lastPos):
        a=self.get(self.getPosicionRata()[0], self.getPosicionRata()[1])
        m=0
        n=0
        memoria=0
        for k in range(len(a)):
            if k==3:
                n=-1
            elif k==2:
                m=-1
            elif k==1:
                n=1
            elif k==0:
                m=1
            if type(self.ultimaPos(m, n, lastPos))==tuple:
                memoria=self.ultimaPos(m, n, lastPos)
            if a[-1*(k-3)]==False:
                b=self.getInfoCelda(self.getPosicionRata()[0]+m, self.getPosicionRata()[1]+n)
                if b['visitada']==False and b['caminoActual']==False:
                    self.diccionarios[self.getPosicionRata()[0]][self.getPosicionRata()[1]]['caminoActual']=True
                    self._redibujar()
                    lastPos=self.getPosicionRata()
                    self.setPosicionRata(self.getPosicionRata()[0]+m, self.getPosicionRata()[1]+n)
                    self._redibujar()
                    if self.resuelto():
                        return True
                    else:
                        return self.resolverAux(lastPos) #aplico resolverAux recursivamente
            m=0
            n=0
            if k==3:
                self.retroceder(lastPos)
                lastPos=memoria
                if memoria==0:
                    return self.resuelto()
                return self.resolverAux(lastPos)
    
    def resolver(self):
        lastPos=(0, 0)
        return self.resolverAux(lastPos)

    def _redibujar(self):
        if self.parent is not None:
            self.parent.update()
